import "babel-polyfill";

import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import Counter from "./Counter";
import reducer from "./reducers";
import rootSaga from "./sagas";

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

const action = type => store.dispatch({ type });

function render() {
  ReactDOM.render(
    <Counter
      value={store.getState().count}
      onIncrementAsync={() => {
        console.log(Date.now());
        action("INCREMENT_ASYNC");
      }}
      onIncrementAsyncOnlyOne={() => {
        console.log(Date.now());
        action("INCREMENT_ASYNC_ONLY_ONE");
      }}
      onIncrement={() => action("INCREMENT")}
      onDecrement={() => action("DECREMENT")}
      loggedIn={store.getState().loggedIn}
      onLogin={() => action("LOGIN")}
      onLogout={() => action("LOGOUT")}
    />,
    document.getElementById("root")
  );
}

render();
store.subscribe(render);

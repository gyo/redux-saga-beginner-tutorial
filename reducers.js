export default function reducer(state = { count: 0, loggedIn: false }, action) {
  switch (action.type) {
    case "INCREMENT":
      return {
        ...state,
        count: state.count + 1
      };
    case "INCREMENT_IF_ODD":
      return {
        ...state,
        count: state.count % 2 !== 0 ? state.count + 1 : state.count
      };
    case "DECREMENT":
      return {
        ...state,
        count: state.count - 1
      };
    case "LOGIN":
      return {
        ...state,
        loggedIn: true
      };
    case "LOGOUT":
      return {
        ...state,
        loggedIn: false
      };
    default:
      return {
        ...state
      };
  }
}

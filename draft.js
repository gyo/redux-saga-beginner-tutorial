const someAsyncFunction = () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve("Finish");
    }, 1000);
  });
};

function* draftFetch() {
  const result = yield someAsyncFunction();
  console.log(result);
}

const iterator = draftFetch();

// console.log(iterator.next());
// console.log(iterator.next());

////////////////////////////////////////////////////////////

const consoleNow = () => {
  const now = Date.now();
  console.log(now);
  return now;
};

function* whileGenerator() {
  while (true) {
    yield consoleNow();
  }
}

const whileIterator = whileGenerator();

// whileIterator.next();
// whileIterator.next();

function* forLoopGenerator() {
  for (let i = 0; i < 2; i++) {
    console.log("@inside forLoopGenerator");
    yield consoleNow();
  }
  yield console.log("Over 2 loop");
}

const forLoopIterator = forLoopGenerator();

// console.log(forLoopIterator.next());
// console.log(forLoopIterator.next());
// console.log(forLoopIterator.next());
// console.log(forLoopIterator.next());

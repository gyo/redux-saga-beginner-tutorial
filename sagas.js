import {
  put,
  take,
  takeEvery,
  takeLatest,
  all,
  call
} from "redux-saga/effects";

export const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

function* watchAndLog() {
  // yield takeEvery("*", function* logger(action) {
  //   console.log("action", action);
  // });

  while (true) {
    const action = yield take("*");
    console.log("action", action);
  }
}

export function* helloSaga() {
  console.log("Hello Sagas!");
}

export function* incrementAsync() {
  yield call(delay, 1000);
  yield put({ type: "INCREMENT" });
  console.log(Date.now());
}

export function* watchIncrementAsync() {
  yield takeEvery("INCREMENT_ASYNC", incrementAsync);
}

export function* watchIncrementAsyncOnlyOne() {
  yield takeLatest("INCREMENT_ASYNC_ONLY_ONE", incrementAsync);
}

export function* loginFlow() {
  // 一度 LOGIN すると、 LOGOUT するまで LOGIN はできなくなる
  while (true) {
    yield take("LOGIN");
    console.log("LOGIN!");
    yield take("LOGOUT");
    console.log("LOGOUT!");
  }
}

export default function* rootSaga() {
  yield all([
    watchAndLog(),
    helloSaga(),
    watchIncrementAsync(),
    watchIncrementAsyncOnlyOne(),
    loginFlow()
  ]);
}

/*eslint-disable no-unused-vars */
import React, { Component, PropTypes } from "react";

const Counter = ({
  value,
  onIncrement,
  onDecrement,
  onIncrementAsync,
  onIncrementAsyncOnlyOne,
  loggedIn,
  onLogin,
  onLogout
}) => (
  <div>
    <div>Clicked: {value} times</div>
    <button onClick={onIncrement}>Increment</button>{" "}
    <button onClick={onDecrement}>Decrement</button>
    <button onClick={onIncrementAsync}>Increment after 1 second</button>{" "}
    <button onClick={onIncrementAsyncOnlyOne}>
      Increment after 1 second （連打無効）
    </button>
    <hr />
    {/* {loggedIn ? ( */}
    <button onClick={onLogout}>Logout</button>
    {/* ) : ( */}
    <button onClick={onLogin}>Login</button>
    {/* )} */}
  </div>
);

Counter.propTypes = {
  value: PropTypes.number.isRequired,
  onIncrement: PropTypes.func.isRequired,
  onDecrement: PropTypes.func.isRequired
};

export default Counter;
